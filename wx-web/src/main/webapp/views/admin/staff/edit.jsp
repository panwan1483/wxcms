<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">


    <title>后台管理系统</title>
    <% String path = request.getContextPath(); %>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css" media="all">

</head>
<body>
<div class="layui-form" >
<div class="container">

    <form id="staffForm" method="post" class="layui-form" action="">
           <input id="staff_pk" name="staff_pk" type="hidden" />
           <div class="layui-form-item">
               <label class="layui-form-label">角色名称</label>
               <div class="layui-input-block">
                   <select id="rolelist" name="rolelist" lay-verify="required">
                       <option value="">请选择角色名称</option>
                   </select>
               </div>
           </div>

           <div class="layui-form-item" style="margin: 5% auto">
               <label class="layui-form-label">姓名</label>
               <div class="layui-input-block">
                   <input id="staff_name" name="staff_name"  lay-verify="required"  autocomplete="off" placeholder="请输入姓名" class="layui-input" type="text" />
               </div>
           </div>
           <div class="layui-form-item">
               <label class="layui-form-label">性别</label>
               <div class="layui-input-block">
                   <input type="radio" id="staff_sex" name="staff_sex" value="1" title="男" checked  >
                   <input type="radio" id="staff_female" name="staff_sex" value="0" title="女" >
               </div>
           </div>
           <div class="layui-form-item">
               <label class="layui-form-label">登录名</label>
               <div class="layui-input-block">
                   <input id="staff_loginname" name="staff_loginname" lay-verify="required" autocomplete="off" placeholder="请输入登录名" class="layui-input" type="text">
               </div>
           </div>
           <div class="layui-form-item">
               <label class="layui-form-label">密码</label>
               <div class="layui-input-block">
                   <input id="staff_password" name="staff_password" lay-verify="required"  autocomplete="off" placeholder="请输入密码" class="layui-input" type="password" >
               </div>
           </div>
           <div class="layui-form-item">
               <label class="layui-form-label">确认密码</label>
               <div class="layui-input-block">
                   <input id="staff_repassword" name="staff_repassword"  lay-verify="staff_repassword"  autocomplete="off" placeholder="请输入确认密码" class="layui-input" type="password">
               </div>
           </div>


           <div class="layui-form-item">
               <label class="layui-form-label">手机号码</label>
               <div class="layui-input-block">
                   <input id="staff_phone" name="staff_phone" autocomplete="off" placeholder="请输入手机号码" class="layui-input" type="text" />
               </div>
           </div>

           <div class="layui-form-item">
               <label class="layui-form-label">电子邮箱</label>
               <div class="layui-input-block">
                   <input id="staff_email" name="staff_email" autocomplete="off" placeholder="请输入电子邮箱" class="layui-input" type="text" />
               </div>
           </div>

            <div class="layui-form-item">
                <label class="layui-form-label">出生日期</label>
                <dvi class="layui-input-inline">
                    <input id="staff_birthday" name="staff_birthday" class="layui-input" type="text" placeholder="自定义日期格式">
                </dvi>
            </div>

           <div class="layui-form-item">
               <label class="layui-form-label">录入时间</label>
               <dvi class="layui-input-inline">
                   <input id="staff_intime" name="staff_intime" class="layui-input" type="text" placeholder="自定义日期格式">
               </dvi>
           </div>


        <div class="layui-form-item" pane="">
        <label class="layui-form-label">是否启用</label>
        <div class="layui-input-block">
        <input id="staff_status0" name="staff_status" value="0" title="启用" checked type="radio"/>
        <input id="staff_status1" name="staff_status" value="1" title="禁用" type="radio"/>
        </div>
        </div>
           <div class="layui-form-item">
               <div align="center" class="layui-input-block" style="margin: 5% auto">
                   <button class="layui-btn layui-btn-small" align="center" id="edit">保存</button>
                   <button class="layui-btn layui-btn-small" id="reset" align="center" type="reset">重置</button>
               </div>
           </div>

    </form>
</div>
</div>
</body>


<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<%--layui--%>
<script>
    layui.use(['form','laydate'],function () {
        var $ = layui.jquery;
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#staff_birthday' //指定元素
        });
        laydate.render({
            elem: '#staff_intime' //指定元素
        });

        $.getJSON("<%=path%>/role/list", function(response) {
            var json = eval(response); // 数组
            $.each(json, function(index, item) {
                var role_pk = json[index].role_pk;
                var role_name = json[index].role_name;
                $("#rolelist").append("<option value=" + role_pk + ">" + role_name + "</option>"); // 添加一项option
//                $(".selects").find("option[value='"+json[index].role_pk+"']").prop("checked","checked");
            });
            form.render('select');

        });



        $(function (){
            var staff_pk=$("#staff_pk").val();
            if(staff_pk!=null && staff_pk!=''){
               $.ajax({
                   type:'post',
                   url:"<%=path%>/staff/list",
                   data:{staff_pk:staff_pk},
                   success:function (response) {
                       var json = eval(response); // 数组
                       $.each(json,function (i) {
                           $("#staff_pk").val(json[i].staff_pk);
                           $("#rolelist").val(json[i].fk_roles_pk);
                           $("#staff_name").val(json[i].staff_name);
                           $("#staff_sex").val(json[i].staff_sex);
                           $("#staff_loginname").val(json[i].staff_loginname);
                           $("#staff_password").val(json[i].staff_password);
                           $("#staff_repassword").val(json[i].staff_password);
                           $("#staff_phone").val(json[i].staff_phone);
                           $("#staff_email").val(json[i].staff_email);
                           $("#staff_birthday").val(json[i].staff_birthday);
                           $("#staff_intime").val(json[i].staff_intime);
                           $(":radio[name='staff_sex'][value='" + json[i].staff_sex + "']").prop("checked", "checked");
                           $(":radio[name='staff_status'][value='"+json[i].staff_status+"']").prop("checked","checked");
                       });
                       form.render('select');
                       form.render('radio');
                   }
               });
            }
        });


        $("#edit").on('click',function(){
            var url="<%=path%>/staff/save";
            var staff_pk=$("#staff_pk").val();
            var staff_name=$("#staff_name").val();
            var staff_loginname=$("#staff_loginname").val();
            var staff_password=$("#staff_password").val();
            var role_id =$("#rolelist").val();
            form.verify({
                staff_repassword : function (value) {
                    var staff_password=$("#staff_password").val();
                    if(staff_password != value){
                        return '两次输入的密码不一致';
                    }
                    $.ajax({
                        type : 'post',
                        async : false,//同步请求，执行成功后才会继续执行后面的代码
                        url : url,
                        data:{
                            staff_pk:staff_pk,
                            staff_name:staff_name,
                            staff_loginname:staff_loginname,
                            staff_password:staff_password,
                            fk_roles_pk:role_id,
                            staff_phone:$("#staff_phone").val(),
                            staff_email:$("#staff_email").val(),
                            staff_intime:$("#staff_intime").val(),
                            staff_birthday:$("#staff_birthday").val(),
                            staff_sex:$("input[name='staff_sex']:checked").val(),
                            staff_status:$("input[name='staff_status']:checked").val()
                        },
                        success:function(response){
                            parent.layer.alert(response.msg);
                            window.location.reload();
                        },
                        error:function (response) {
                            parent.layer.alert(response.msg);
                            window.location.reload();
                        }
                    });
                    return false;
                }
            });

        });

    })

</script>
</html>
